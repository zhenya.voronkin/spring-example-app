package ru.spring.course;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class MusicPlayer {

  @Value("${music-player.name}")
  private String name;
  @Value("${music-player.volume}")
  private int volume;
  private final Music music1;
  private final Music music2;

  public String getName() {
    return name;
  }

  public int getVolume() {
    return volume;
  }

  public MusicPlayer(
          @Qualifier("rockMusic") Music music1,
          @Qualifier("classicalMusic") Music music2
  ) {
    this.music1 = music1;
    this.music2 = music2;
  }

  public String playMusic() {
    return "Playing: " + music1.getSong() + ", " + music2.getSong();
  }

}
