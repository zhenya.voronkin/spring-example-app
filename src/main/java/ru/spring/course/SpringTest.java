package ru.spring.course;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringTest {

  public static void main(String[] args) {
    final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
            Config.class
    );

    MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);

    System.out.println(musicPlayer.getName());
    System.out.println(musicPlayer.getVolume());
    System.out.println(musicPlayer.playMusic());

    ClassicalMusic classicalMusic1 = context.getBean("classicalMusic", ClassicalMusic.class);
    ClassicalMusic classicalMusic2 = context.getBean("classicalMusic", ClassicalMusic.class);

    System.out.println(classicalMusic1 == classicalMusic2);

    context.close();

  }
}
